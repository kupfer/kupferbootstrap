---
nosearch: true
orphan: true
---

only used to trigger builds of the submodule docs!

```{eval-rst}
.. autosummary::
   :toctree: cli
   :template: command.rst
   :recursive:

    binfmt
    cache
    chroot
    config
    devices
    flavours
    image
    net
    packages
```
